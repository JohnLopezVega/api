//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;


var bodyparser = require('body-parser');
app.use(bodyparser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
var requestJson = require('request-json');

var path = require('path');

const URL_MOVIMIENTOS = "https://api.mlab.com/api/1/databases/jlopez/collections/movimientos";
const API_KEY = "?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname, "index.html"));
});

app.post("/", function(req, res) {
  res.send("Hemos recibido su peticion post");
});

app.get("/clientes/:idcliente", function(req, res) {
  var mijson = {
    idcliente: 12345
  };
  res.send(mijson);
});

app.put("/", function(req, res) {
  res.send("hemos recibido su peticion put cambiado");
});

app.delete("/", function(req, res) {
  res.send("hemo recibido su peticion delete");
});

app.get("/v1/movimientos", function(req, res) {
  console.log(req.query);
  res.sendFile(path.join(__dirname, "./movimientos1.json"));
});

var movimientosJSON = require("./movimientos2.json");

app.get("/v2/movimientos", function(req, res) {
  console.log(req.query);
  res.json(movimientosJSON);
});

app.get("/v2/movimientos/:id", function(req, res) {
  console.log(req.params.id);
  res.send(movimientosJSON[req.params.id - 1]);
});

app.get("/v2/movimientosq", function(req, res) {
  console.log(req.query);
  res.send("recibido");
});

app.post("/v2/movimientos", function(req, res) {
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
  res.send("Movimientos dado de alta");
});

app.get("/v3/movimientos", function (req, res) {
  var clienteMlab = requestJson.createClient(URL_MOVIMIENTOS + API_KEY);
  clienteMlab.get("", function(err, resM, body) {
    if (err) {
      console.log(err);
    } else {
      res.setHeader('Content-Type', 'application/json');
      res.send(body);
    }
  });
});

app.post("/v3/movimientos", function (req, res) {
  var clienteMlab = requestJson.createClient(URL_MOVIMIENTOS + API_KEY);
  console.log(req.body);
  clienteMlab.post('', req.body, function(err, resM, body) {
    res.setHeader('Content-Type', 'application/json');
    res.send(body);
  });
});